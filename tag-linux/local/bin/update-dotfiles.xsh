from datetime import datetime

changes_string = 'Changes not staged for commit:'
untracked_string = 'Untracked files:'
uncommitted_changes_string = 'Changes to be committed:'

working_tree_clean_string = 'nothing to commit, working tree clean'
branch_ahead_string = 'Your branch is ahead of '

# Update from repo
$[git pull]

# Check initial status
git_status_stdout = $(git status)

while_counter = 0 # Used to break out of the while loop if it repeats too many times.
while working_tree_clean_string not in git_status_stdout:
    # Check status
    git_status_stdout = $(git status)

    while_counter += 1

    if (changes_string in git_status_stdout) or (untracked_string in git_status_stdout):
        print('Changes made. Staging changes')
        # Add
        $[git add .]
    elif uncommitted_changes_string in git_status_stdout:
        $commit_message = "Committed at: " + str(datetime.now())
        print('Committing changes')
        # Commit
        $[git commit -m $commit_message]
    elif branch_ahead_string in git_status_stdout:
        print('Pushing commits')
        # Push
        $[git push]
    else:
        raise RuntimeError("Couldn't determine git state")

    if while_counter > 5:
        raise RuntimeError('Loop repeated too many times. Stopping.')

