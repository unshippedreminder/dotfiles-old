# Personal Dotfiles

Personal dotfiles managed by RCM

## Usage

Clone Repo:
```
git clone https://gitlab.com/unshippedreminder/dotfiles ~/.dotfiles
```
Run rcm:
```
rcup -v
```