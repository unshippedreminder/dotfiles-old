aliases |= {
    # ls
    'ls': 'exa --long --group-directories-first --git --header --icons --classify',
    'la': 'exa --long --group-directories-first --git --header --icons --classify --all',
    'lt': 'exa --long --group-directories-first --git --header --icons --classify --tree --level 2',
    # Run in Toolbox
    'tldr': 'toolbox run --container fedora-toolbox-custom tldr',
    'pip': 'toolbox run --container fedora-toolbox-custom pip',
    'git': 'toolbox run --container fedora-toolbox-custom git',
    ## rcm
    'lsrc': 'toolbox run --container fedora-toolbox-custom lsrc',
    'mkrc': 'toolbox run --container fedora-toolbox-custom mkrc',
    'rcdn': 'toolbox run --container fedora-toolbox-custom rcdn',
    'rcup': 'toolbox run --container fedora-toolbox-custom rcup',
    # cd
    '..': 'cd ..',
    '...': 'cd ../..',
    '....': 'cd ../../..',
    # find > fd
    'find': 'fd',
    # Toolbox
    'tr': 'toolbox run',
    'te': 'toolbox enter',
    'tc': 'toolbox create',
    # Ansible
    'ap': 'ansible-playbook',
    'ag': 'ansible-galaxy',
    # RPM-OSTree
    'rot': 'rpm-ostree',
    'roti': 'rpm-ostree install',
    'rotu': 'rpm-ostree upgrade',
    'rotr': 'rpm-ostree remove',
    # ripgrep
    'rg': 'rg --smart-case',
    # less > most
    'less': 'most'
}
