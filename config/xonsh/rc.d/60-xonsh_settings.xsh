$CASE_SENSITIVE_COMPLETIONS = False

# Make VTE terminal tabs start in the correct directory
# https://xon.sh/customization.html#make-terminal-tabs-start-in-the-correct-directory
$PROMPT = '{vte_new_tab_cwd}' + $PROMPT
