" ###############################
" ###         PLUGINS         ###
" ###############################

" Autoinstall vim-plug
" Docs: https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin(stdpath('data') . '/plugged')

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/preservim/nerdtree     
Plug 'preservim/nerdtree' " Tree-folder-view - Command `:NERDTree` 
Plug 'Xuyuanp/nerdtree-git-plugin' " Git icons for NERDTree
Plug 'ryanoasis/vim-devicons' " Adds file type icons to Vim plugins such as: NERDTree, vim-airline, CtrlP, unite, Denite, lightline, vim-startify and many more 
Plug 'scrooloose/syntastic' " Syntax Checking
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' } " Autocompletion - Needs `msgpack` from pip
Plug 'tpope/vim-fugitive' " Git plugin - Usage `:Git`/`:G`
Plug 'airblade/vim-gitgutter' " git diff in sign column (line number column)
Plug 'vim-airline/vim-airline' " Powerline-like statusline
Plug 'vim-airline/vim-airline-themes' " vim-airline themes - Usage: `:AirlineTheme <theme>` eg. `:AirlineTheme murmur`
Plug 'neovim/nvim-lspconfig' "Quickstart configurations for the Nvim LSP client
Plug 'williamboman/nvim-lsp-installer' "Neovim plugin that allows you to seamlessly manage LSP servers with :LspInstall.
Plug 'morhetz/gruvbox' "Gruvbox theme
Plug 'edkolev/tmuxline.vim' " Syncs NVims Airline statusline colors to TMux
" Initialize plugin system
call plug#end()

" ###############################
" ###        PLUGINS END      ###
" ###############################

" Disable overwriting of tmux theme (because it's manually configured)
" https://github.com/edkolev/tmuxline.vim#use-vim-airline-colors
let g:airline#extensions#tmuxline#enabled = 0

" Deoplete - Docs: https://github.com/Shougo/deoplete.nvim
let g:deoplete#enable_at_startup = 1

" Gruvbox
autocmd vimenter * ++nested colorscheme gruvbox " Needed by Gruvbox theme

" Importing lua config
lua require('config')

" Tabs
filetype plugin indent on " use language‐specific plugins for indenting

" NerdTree Sidepanel
autocmd VimEnter * NERDTree
autocmd BufEnter * NERDTreeMirror
" Next line seems to apply default behavior, but leaving it in, in case i'm wrong
"autocmd VimEnter * NERDTreeCWD " Set NERDTree root directory to Current Working Directory

" If more than one window and previous buffer was NERDTree, go back to it.
autocmd BufEnter * if bufname('#') =~# "^NERD_tree_" && winnr('$') > 1 | b# | endif

" Avoid crashes when calling vim-plug functions while the cursor is on the NERDTree window
let g:plug_window = 'noautocmd vertical topleft new'

" Close NERDTree if the file windows closes
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") 
    \ && b:NERDTree.isTabTree()) | q | endif
" NerdTree-Git settings
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'𝚫',
                \ 'Staged'    :'',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'⤧',
                \ 'Deleted'   :'',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }
let g:NERDTreeGitStatusUseNerdFonts = 1

" Terminal Panel
autocmd TermOpen * setlocal nonumber norelativenumber " Don't show line numbers in terminal
autocmd TermOpen * startinsert
"  autocmd BufWinEnter,WinEnter term://* startinsert " Go to insert mode when entering terminal window
autocmd BufLeave term://* stopinsert " Return to normal mode when leaving a terminal

" Toggle 'default' terminal
nnoremap <C-t> :call ChooseTerm("term-slider", 1)<CR>
" Start terminal in current pane
"nnoremap <C-p> :call ChooseTerm("term-pane", 0)<CR>

function! ChooseTerm(termname, slider) " Pop-up terminal
  let pane = bufwinnr(a:termname)
  let buf = bufexists(a:termname)
  if pane > 0
    " pane is visible
    if a:slider > 0
      :exe pane . "wincmd c"
    else
      :exe "e #" 
    endif
  elseif buf > 0
    " buffer is not in pane
    if a:slider
      :exe "topleft 15split"
    endif
    :exe "buffer " . a:termname
  else
    " buffer is not loaded, create
    if a:slider
      :exe "topleft 15split"
    endif
    :terminal
    :exe "f " a:termname
  endif
endfunction
autocmd VimEnter * wincmd w " Start with cursor in file editing window


" Filetypes
"autocmd BufRead,BufNewFile *.xsh setf python

" Skeleton Files
":autocmd BufNewFile  *.service	0r ~/.config/nvim/templates/skeleton.service
":autocmd BufNewFile  *.timer	0r ~/.config/nvim/templates/skeleton.timer
"
":autocmd BufNewFile  *.container	0r ~/.config/nvim/templates/skeleton.container
":autocmd BufNewFile  *.network	0r ~/.config/nvim/templates/skeleton.network
":autocmd BufNewFile  *.volume	0r ~/.config/nvim/templates/skeleton.volume
":autocmd BufNewFile  *.kube	0r ~/.config/nvim/templates/skeleton.kube
"
":autocmd BufNewFile  *.sh	0r ~/.config/nvim/templates/skeleton.sh
":autocmd BufNewFile  *.py	0r ~/.config/nvim/templates/skeleton.py
